package br.com.efaber.architecture.ee7ejb.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;

import br.com.efaber.architecture.ee7ejb.model.IEntityBasic;

@Named
@Stateless
public class PersistenceBasic<T extends IEntityBasic> implements IPersistenceBasic<T> {

	private static final long serialVersionUID = -7985410885374189615L;

	@Inject
	private EntityManager entityManager;

	@Override
	public void saveOrUpdate(T pojo) {
		if (pojo.getId() == null) {
			entityManager.persist(pojo);
		} else {
			entityManager.merge(pojo);
		}
	}

	@Override
	public void saveOrUpdate(List<T> pojos) {
		for (T pojo : pojos) {
			saveOrUpdate(pojo);
		}
		entityManager.flush();
	}

	@Override
	public void remove(T pojo) {
		entityManager.remove(pojo);
	}

	@Override
	public T find(Class<T> clazz, Object id) {
		return entityManager.find(clazz, id);
	}

	@Override
	public Criteria createCriteria(Class<T> clazz, String alias) {
		Session session = getSession();
		return session.createCriteria(clazz, alias);
	}

	@Override
	public T getSingleResult(Criteria criteria) {
		criteria.setMaxResults(1);
		List<T> pojoList = getResultList(criteria);
		if (!pojoList.isEmpty()) {
			return pojoList.get(0);
		}
		return null;
	}

	@Override
	public List<T> getResultList(Criteria criteria) {
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unchecked")
		List<T> pojoList = criteria.list();
		return pojoList;
	}

	@Override
	public void remove(Class<T> clazz, T pojo) {
		entityManager.refresh(pojo);
		entityManager.remove(pojo);
	}

	private Session getSession() {
		Session session = (Session) entityManager.getDelegate();
		return session;
	}

}
