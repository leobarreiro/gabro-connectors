package br.com.efaber.architecture.ee7ejb.persistence;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import org.hibernate.Criteria;

import br.com.efaber.architecture.ee7ejb.model.IEntityBasic;

@Local
public interface IPersistenceBasic<T extends IEntityBasic> extends Serializable {

	/**
	 * Persiste o objeto na base. Se o Pojo possuir um Id, realiza um merge. Caso contrário, realiza um persist.
	 * 
	 * @param pojo
	 *            Method Parameter.
	 */
	void saveOrUpdate(T pojo);

	/**
	 * Persiste uma lista de objetos na base.
	 * 
	 * @param pojos
	 *            Method Parameter.
	 */
	void saveOrUpdate(List<T> pojos);

	/**
	 * Remove um objeto da base.
	 * 
	 * @param pojo
	 *            Method Parameter.
	 */
	void remove(T pojo);

	/**
	 * Realiza uma busca no banco pelo objeto com o Id especificado.
	 * 
	 * @param clazz
	 *            Method Parameter.
	 * @param id
	 *            Method Parameter.
	 * @return Objeto que deriva de IPojo
	 */
	T find(Class<T> clazz, Object id);

	/**
	 * Cria uma Criteria do Hibernate com alias.
	 * 
	 * @param clazz
	 *            Method Parameter.
	 * @param alias
	 *            Method Parameter.
	 * @return criteria Hibernate
	 */
	Criteria createCriteria(Class<T> clazz, String alias);

	/**
	 * Retorna o objeto tipado, resultante da pesquisa efetuada na criteria.
	 * 
	 * @param criteria
	 *            Method Parameter.
	 * @return objeto derivado de IPojo
	 */
	T getSingleResult(Criteria criteria);

	/**
	 * Retorna uma lista de objetos tipados de acordo com a criteria estabelecida.
	 * 
	 * @param criteria
	 *            Method Parameter.
	 * @return lista de objetos tipados
	 */
	List<T> getResultList(Criteria criteria);

	/**
	 * Deleta um objeto da base.
	 * 
	 * @param clazz
	 *            Method Parameter.
	 * @param pojo
	 *            Method Parameter.
	 */
	void remove(Class<T> clazz, T pojo);

}
