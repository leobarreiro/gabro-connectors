package br.com.efaber.architecture.ee7ejb.model;

import java.io.Serializable;

public interface IEntityBasic extends Serializable {

	Object getId();

}
